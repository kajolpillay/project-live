<?php
function coming_soon_enqueue_style() {

wp_enqueue_style( 'coming-soon-style', get_stylesheet_uri() . 'style.css' );
wp_enqueue_script('coming-soon-script', get_template_directory_uri() . '/assets/js/main.js');
   
}

add_action( 'wp_enqueue_scripts', 'coming_soon_enqueue_style' );

function coming_soon_setup() {
    add_theme_support('title-tag');
    add_theme_support( 'custom-logo' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-header' );
}

add_action('after_setup_theme', 'coming_soon_setup'); 

function coming_soon_activated() {
    update_option('show_on_front', 'page');
      update_option('page_on_front', 701);
}

add_action(' after_switch_theme', 'coming_soon_activateds'); 

function css_background_image_override($selector, $image) {

    $style = '';

    $template = "<style>{selector}{background-image:url('{image}') }</style>";

    if ( $image ) {

        $style = $template;

        $style = str_replace('{selector}', $selector, $style);

        $style = str_replace('{image}', $image, $style);

    }

    echo $style;

}

