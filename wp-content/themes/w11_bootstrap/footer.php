<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package w11_bootstrap
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<nav class="social-navigation" role="navigation">

<?php

	wp_nav_menu( array(

	'theme_location' => 'menu-social',

	'menu_class' => 'social-links-menu'

	) );

?>

<nav class="footer-navigation" role="navigation">

<?php

	wp_nav_menu( array(

	'theme_location' => 'menu-footer',

	'menu_class' => 'footer-links-menu'

	) );

?>



</nav>
</div>
</footer>
</div>

</body>
</html>
